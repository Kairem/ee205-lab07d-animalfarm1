///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file deleteCats.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

extern void deleteAllCats();

//@todo add function deleteCat(index) that removes an index from the arrays

