///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

extern int updateCatName( int index, char newName[] );

extern int fixCat( int index );

extern int updateCatWeight( int index, float newWeight );

extern bool updateCatCollar1( int index, enum Color collar1 );

extern bool updateCatCollar2( int index, enum Color collar2 );

extern bool updateLicense( int index, unsigned long long license );

extern bool updateCatBirthday( int index, char bday[] );

