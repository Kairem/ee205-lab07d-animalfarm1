///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file main.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

#define DEBUG

int main() {

   addCat( "Loki", MALE, PERSIAN, true, 8.5, BLACK, WHITE, 101, "June 9, 2012" ) ;
   addCat( "Milo", MALE, MANX, true, 7.0, BLACK, RED, 102, "May 1, 2000" ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2, BLACK, BLUE, 103, "December 4, 2016" ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2, BLACK, GREEN, 104, "November 22, 1999" ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2, BLACK, PINK, 105, "July 4, 2014" ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0, WHITE, BLACK, 106, "September 8, 2003" ) ;
   printAllCats();

   updateCatCollar1( 1, GREEN );
   printCat( 1 );

   int kali = findCat( "Kali" ) ;
   
   #ifdef DEBUG
   printf("Kali's index: [%d] \n", kali);
   #endif

   printCat( kali );
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );
   printAllCats();
   deleteAllCats();
   printAllCats();


   #ifdef DEBUG
   
   addCat( "Loki", MALE, PERSIAN, true, 8.5, BLACK, WHITE, 101, "June 9, 2012" );
   addCat( "Milo", MALE, MANX, true, 7.0, BLACK, RED, 102, "July 20, 2012" );

   printCat( findCat( "Loki" ) );
   
   updateCatName( findCat( "Loki" ), "Milo" );  //should cause error [cat name already in database]
  
   updateCatName( findCat( "Loki" ), "Yolo" );
   
   printCat( -1 );
   printCat( findCat( "Yolo" ) );
   printCat( findCat( "Loki" ) );   //error

   printCat( findCat( "Milo" ) );

   assert( updateLicense( 1, 101 ) == false );
   assert( updateLicense( 1, 103 ) == true );
   
   assert( updateCatCollar2( 1, WHITE ) == false );
   assert( updateCatCollar1( 1, WHITE ) == true );

   deleteAllCats();

   #endif
}
