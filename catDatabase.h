///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <time.h>

#define MAX_CAT 1024
#define MAX_CAT_NAME 50

enum Gender {UNKNOWN_GENDER, MALE, FEMALE};

enum Breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

enum Color {BLACK, WHITE, RED, BLUE, GREEN, PINK};

struct Cat {

   char name[MAX_CAT_NAME];
   enum Gender gender;
   enum Breed breed;
   bool isFixed;
   float weight;
   enum Color collarColor1;
   enum Color collarColor2;
   unsigned long long license;
   struct tm birthday;

};


extern struct Cat catArray[MAX_CAT];

extern int numCats;


extern char* genderName( enum Gender gender );

extern char* breedName( enum Breed breed );

extern char* colorName( enum Color color );


extern bool nameIsValid( char name[] );

extern bool nameInDatabase( char name[] );

extern bool weightIsValid( float weight );

extern bool isFull();

extern bool indexIsValid( int index );

extern bool collarsAreValid( enum Color collar1, enum Color collar2 );

extern bool licenseIsValid( unsigned long long license );

//@todo add initialize database funciton that memsets the arrays to zero

