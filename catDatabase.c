///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

struct Cat catArray[MAX_CAT]; //Cat Database

int numCats = 0;              //The high water mark for the array (The number of cats in the array)


////////////////////  Enum Functions  ////////////////////

char* genderName( enum Gender gender ) {

   switch (gender) {
      case 0:
         return "Unknown Gender";
         break;
      case 1:
         return "Male";
         break;
      case 2:
         return "Female";
         break;
      default:
         return "Unknown Gender";
         break;
   }

}


char* breedName( enum Breed breed ) {
   
   switch (breed) {
      case 0:
         return "Unknown Breed";
         break;
      case 1:
         return "Maine Coon";
         break;
      case 2:
         return "Manx";
         break;
      case 3:
         return "Shorthair";
         break;
      case 4:
         return "Persian";
         break;
      case 5:
         return "Sphynx";
         break;
      default:
         return "Unknown Gender";
         break;
   }

}


char* colorName( enum Color color ) {

   switch (color) {
      case 0:
         return "Black";
         break;
      case 1:
         return "White";
         break;
      case 2:
         return "Red";
         break;
      case 3:
         return "Blue";
         break;
      case 4:
         return "Green";
         break;
      case 5:
         return "Pink";
         break;
      default:
         return "Unknown Color";
         break;
   }

}


////////////////////  Validation Functions  ////////////////////

bool nameIsValid( char name[] ) {     
   
   if( strlen(name) < 1 ) {

      fprintf( stderr, "Cat name cannot be empty.\n" );
      return false;
   
   }

   if ( strlen(name) > MAX_CAT_NAME ) {

      fprintf( stderr, "Cat name [%s] must be shorter than or equal to [%d] characters.\n", name, MAX_CAT_NAME );
      return false;

   }

   return true;

}


bool nameInDatabase( char name[] ) {

   for ( int i = 0; i < numCats; i++ ) {

      if( strcmp(catArray[i].name, name) == 0 ) {

         fprintf( stderr, "Cat name [%s] already in database.\n", name );
         return true;

      }

   }

   return false;

}


bool weightIsValid( float weight ) {
   
   if ( weight <= 0 ) {

      fprintf( stderr, "Cat weight [%f] must be greater than zero.\n", weight );
      return false;

   }

   return true;

}


bool isFull() {

   if ( numCats >= MAX_CAT ) { 
      
      fprintf( stderr, "Database is Full.\n" );
      return true;
   
   }
   
   return false;

} 


bool indexIsValid( int index ) {

   if( numCats == 0 ) {
      fprintf( stderr, "Cat Database is empty.\n" );
      return false;
   }
   
   if( index < 0 ) {
      fprintf( stderr, "Animal Farm 1: Bad Cat! Index [%d] must be greater than zero.\n", index );
      return false;
   }

   if( index > numCats ) {
      fprintf( stderr, "Animal Farm 1: Bad Cat! Index [%d] must be less than [%d]\n", index, numCats );
      return false;
   }

   return true;

}


bool collarsAreValid( enum Color collar1, enum Color collar2 ) {
   
   if( collar1 == collar2 ) {
      fprintf( stderr, "Cat Collars cannot be the same color.\n" );
      return false;
   }

   for( int i = 0; i < numCats; i++ ) {

      if( catArray[i].collarColor1 == collar1 && catArray[i].collarColor2 == collar2 ) {
         fprintf( stderr, "Cat with duplicate collars already in database.\n" );
         return false;
      }

   }

   return true;

}


bool licenseIsValid( unsigned long long license ) {

   for( int i = 0; i < numCats; i++ ) {

      if( catArray[i].license == license ) {
         fprintf( stderr, "Duplicate cat license already in database.\n" );
         return false;
      }

   }

   return true;

}

