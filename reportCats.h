///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////


extern int printCat( int index );

extern void printAllCats();

extern int findCat( char n[] );


