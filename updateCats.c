///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "catDatabase.h"
#include "updateCats.h"

int updateCatName( int index, char newName[] ) {
   
   
   if( ! nameIsValid(newName) || nameInDatabase(newName) || ! indexIsValid(index) ) {
      return 0;
   }

   else {
      strcpy(catArray[index].name, newName);
   }

   return 1;

}


int fixCat( int index ) {

   if( ! indexIsValid(index) ) {
      return 0;
   }
   
   else {
      catArray[index].isFixed = true;
   }

   return 1;

}


int updateCatWeight( int index, float newWeight ) {
   
   if( ! indexIsValid(index) || ! weightIsValid(newWeight) ) {
      return 0;
   }
   
   else {
      catArray[index].weight = newWeight;
   }

   return 1;

}


bool  updateCatCollar1( int index, enum Color collar1 ) {

   if( collarsAreValid(collar1, catArray[index].collarColor2) && indexIsValid(index) ) {
      
      catArray[index].collarColor1 = collar1;
      return true;

   }

   return false;

}


bool updateCatCollar2( int index, enum Color collar2 ) {

   if( collarsAreValid(catArray[index].collarColor1, collar2) && indexIsValid(index) ) {

      catArray[index].collarColor2 = collar2;
      return true;

   }

   return false;

}

bool updateLicense( int index, unsigned long long license ) {

   if( licenseIsValid(license) && indexIsValid(index) ) {
   
      catArray[index].license = license;
      return true;

   }

   return false;

}


bool updateCatBirthday( int index, char bday[] ) {

   struct tm birthday;
   memset(&birthday, 0, sizeof(birthday)-1);

   strptime(bday, "%b %d, %Y", &birthday);

   catArray[index].birthday = birthday;

}

