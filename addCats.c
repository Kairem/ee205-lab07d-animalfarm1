///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Kai Matsusaka <kairem@hawaii.edu>
/// @date 03_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include "catDatabase.h"
#include "addCats.h"

int addCat( char n[], enum Gender g, enum Breed b, bool fix, float w, enum Color collar1, enum Color collar2, unsigned long long license, char bday[] ) {


   if( isFull() || nameInDatabase(n) || ! nameIsValid(n) || ! weightIsValid(w) || ! collarsAreValid(collar1, collar2) || ! licenseIsValid(license) ) {

      return 0;
   
   }

   else {

      strcpy( catArray[numCats].name, n );
      catArray[numCats].gender = g;
      catArray[numCats].breed = b;
      catArray[numCats].isFixed = fix;   
      catArray[numCats].weight = w;      
      catArray[numCats].collarColor1 = collar1;
      catArray[numCats].collarColor2 = collar2;
      catArray[numCats].license = license;
      

      struct tm birthday;
      memset(&birthday, 0, sizeof(birthday)-1);

      strptime(bday, "%b %d, %Y", &birthday);

      catArray[numCats].birthday = birthday;

      numCats++;  //increment the index of the arrays.
   
   }

   return numCats;

}

